import axios from "axios";
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = () => new Vuex.Store({

  state: {
    cars: null,
    availableCategories: null,
    locations: null,
  },
  mutations: {
    GET_MODELS (state, data) {
      state.cars = data.models
      state.availableCategories = data.available_categories
    },
    GET_LOCATION (state, data) {
      state.locations = data.locations
    }
  },
  actions : {
    async nuxtServerInit  ({ commit }) {

      const models = await axios.get('api-1_link');
      const location = await axios.get('api-2_link');

      const data_models = models.data
      const data_location = location.data

      commit('GET_MODELS', data_models )
      commit('GET_LOCATION', data_location )
    },
  }
})

export default store
