export default {
  header__links: {
    location: 'Ou nous trouver?',
    catalog: 'Nos voitures'
  },
  footer_links: {
    terms: 'Conditions générales de location',
    insurance: 'Conditions Générales d’assurance',
  },
  hero: {
    title: "La Location de voitures nouvelle génération",
    subtitle: "Une location de voitures premiums 100% digitale, en toute liberté."
  },
  features: {
    ico_car: {
      name: 'Choisissez votre voiture',
      desc: 'Pas de mauvaises surprises, la voiture (BMW ou Mercedes) que vous conduirez est celle que vous avez choisie'
    },
    ico_dates: {
      name: 'Disponible 24H/24 et 7J/7',
      desc: 'Accédez a votre Toosla de jour comme de nuit'
    },
    ico_location: {
      name: 'Choisissez votre station de départ',
      desc: 'Dites-nous ou vous souhaitez prendre votre voiture Ou faites-vous livrer grâce a notre service de voiturier'
    },
    ico_drive: {
      name: 'En route',
      desc: 'Rendez-vous dans la station Toosla, prenez votre smartphone pour déverrouiller votre voiture. C’est parti'
    }
  },
  rent: {
    title: 'Dans votre smartphone, un choix de voitures Premium suréquipées',
    subtitle: 'En quelques minutes, créez votre compte et réservez pour quelques heures, quelques jours, pour votre prochain weekend ou vos prochaines vacances',
    promise: {
      name: 'Promesse de qualité',
      desc: 'Ajustez votre forfait kilométrique, sélectionnez votre niveau d’assurance, déclarez un second conducteur, Toosla s’adapte'
    },
    digital: {
      name: '100% Digital',
      desc: 'Plus de papiers inutiles, plus d’attente en agence, plus de contraintes dissimulées. Allez directement à votre voiture, faites l’état des lieux et … partez'
    }
  },
  models: {
    title: 'Nos modéles',
    subtitle: 'Le plus dur sera de choisir entre nos 7 modèles Premium super-équipés'
  },
  information: {
    title: 'Pour plus d’informations sur nos modèles'
  },
  looking: {
    title: 'A la recherche du plaisir...',
    subtitle: 'Votre smartphone est votre clé, plus encore, un véritable trousseau vous permettant d’accéder à plusieurs centaines de voitures, toutes suréquipées, toutes plus belles les unes que les autres, plus confortables, plus sures, plus technologiques, pour un seul objectif : votre plaisir',
    text: {
      title: 'Innovation.',
      desc: 'L’innovation est permanente chez Toosla pour que tout soit toujours plus simple, plus rapide, plus intuitif. Sortie de parking par lecture de plaque d’immatriculation, prolongation de location simplifiée, drive mode ajusté ….'
    }

  },
  freedom: {
    title: 'Liberté.',
    desc: 'Nous comprenons que la vie est faite d’imprévus. Prolongez votre weekend, vos vacances directement dans l’onglet « prolonger » sur votre Drive mode ou auprès de notre service client'
  },
  meticulous: {
    title: 'Une sélection exigeante',
    desc: 'Finition AMG Line ou MSport, boite automatique, autonome de niveau 2, toit ouvrant, chargeur de téléphone par induction, Apple car play, … Vivez une expérience différente, voyagez tout en douceur … un pur plaisir'
  },

}
