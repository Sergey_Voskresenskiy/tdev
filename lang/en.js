export default {
  header__links: {
    location: 'Where to find us?',
    catalog: 'Our cars'
  },
  footer_links: {
    terms: 'Terms and conditions a traduire',
    insurance: 'Insurance terms and conditions',
  },
  hero: {
    title: "New generation car rental",
    subtitle: "100% digital premium car rental, with complete freedom."
  },
  features: {
    ico_car: {
      name: 'Pick a car',
      desc: 'Choose from a selection of premium full option cars.'
    },
    ico_dates: {
      name: 'Pick your dates',
      desc: 'Choose your ideal departure and return time. Don’t worry, once started, you can always extend it.'
    },
    ico_location: {
      name: 'Choose a location',
      desc: 'Tell us where you want to pick up your car. It can also get it delivered to your door!'
    },
    ico_drive: {
      name: 'Let’s drive!',
      desc: 'Get to your pickup location and take out your phone to unlock your car. Let’s drive.'
    }
  },
  rent: {
    title: 'In your smartphone, a choice of premium over-equipped cars',
    subtitle: 'In a few minutes, create your account and book for a few hours, a few days, for your next weekend or your next vacation',
    promise: {
      name: 'Promise of quality',
      desc: 'Adjust your mileage package, select your insurance level, declare a second driver, Toosla adapts'
    },
    digital: {
      name: '100% Digital',
      desc: 'No more unnecessary papers, no more waiting in the agency, no more hidden constraints. Go straight to your car, take stock and ... go'
    }
  },
  models: {
    title: 'Our models',
    subtitle: 'The hard part will be choosing between our 7 super-equipped Premium models'
  },
  information: {
    title: 'For more information on our models'
  },
  looking: {
    title: 'In search of pleasure...',
    subtitle: 'Your smartphone is your key, moreover, a real keychain allowing you to access several hundred cars, all over-equipped, each more beautiful than the other, more comfortable, safer, more technological, for a single objective: your pleasure',
    text: {
      title: 'Innovation.',
      desc: 'The entire booking and rental process is done via our mobile application. Your phone becomes your car key. No more spending hours at the counter or filling out multiple forms. Everything is done from your sofa!'
    }
  },
  freedom: {
    title: 'Freedom.',
    desc: 'We understand that life is made up of the unexpected. Extend your weekend, your vacation directly in the "extend" tab on your Drive mode or with our customer service'
  },
  meticulous: {
    title: 'A demanding selection',
    desc: 'AMG Line or MSport finish, automatic gearbox, autonomous level 2, sunroof, induction phone charger, Apple car play, ... Live a different experience, travel smoothly ... pure pleasure'
  },
}
